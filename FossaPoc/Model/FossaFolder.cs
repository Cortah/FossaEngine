﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace FossaPoc.Model
{
    public class FossaFolder : Model
    {
        public string id { get; set; }
        public string label { get; set; }
        public string type { get; set; }

        // Quick access to properties
        // idk if we can find a better way of doing this...
        public FileContents contents { get; set; }
        public FolderProps properties { get; set; }
        public EdgeModel ParentEdge { get; set; }

        #region Functions

        #endregion

        #region Internal Classes
        public class FolderProps : Model
        {
            public string Name() => name[0]["value"];
            public DateTime DateCreated() => DateTime.Parse(createddate[0]["value"]);

            public List<Dictionary<string, string>> name { get; set; }
            public List<Dictionary<string, string>> createddate { get; set; }
        }
        public class FileContents : Model
        {
            public List<FossaFile> files = new List<FossaFile>();
        }
        #endregion
    }

    
}
