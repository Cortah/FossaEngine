﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FossaPoc.Model
{
    public class EdgeModel
    {
        public string id { get; set; }
        public string label { get; set; }
        public string type { get; set; }
        public string inVLabel { get; set; }
        public string outVLabel { get; set; }
        public string inV { get; set; }
        public string outV { get; set; }

        public enum edgeType
        {
            filechild, folderchild
        }
    }
}
