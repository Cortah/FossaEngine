﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace FossaPoc.Model
{
    public class Root : Model
    {
        public string id { get; set; }
        public string label { get; set; }
        public string type { get; set; }

        // Quick access to properties
        // May need to be changed in the future when concidering multiple storage accounts.
        public FolderPath FolderStructure;
        public RootProps properties { get; set; }

        #region InternalFunctions
        #endregion

        #region Internal Classes
        /// <summary>
        /// Properties of the root<para />
        /// This will include All the storage accounts tied to the graph.
        /// </summary>
        public class RootProps : Model
        {
            #region Get Props
            public string Name() => name[0]["value"];
            public string Url() => url[0]["value"];
            public string AccountName() => accountname[0]["value"];
            public string Key() => key[0]["value"];
            #endregion

            public List<Dictionary<string,string>> name { get; set; }
            // Going to replace this with a vertex that will hold storage account info.
            public List<Dictionary<string, string>> url { get; set; }
            public List<Dictionary<string, string>> accountname { get; set; }
            public List<Dictionary<string, string>> key { get; set; }

            // The above will be replaced with....
            // public List<StorageAccounts> orSomethingOrOther;
            // Just need to make a storage account class.
        }
        /// <summary>
        /// This is the class that holds the folder structure.
        /// </summary>
        public class FolderPath : Model
        {
            public List<FossaFolder> path = new List<FossaFolder>();
            public class Path : Model
            {
                // All folders in the database will be collected as a list of strings
                public List<string> objects { get; set; } = new List<string>();
            }
        }
        public class StorageAccount : Model
        {

        }
        #endregion
    }
    
}
