﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace FossaPoc.Model
{
    public class FossaFile : Model
    {
        #region Private variables
        private string id;
        private string label;
        private string type;
        #endregion

        #region Public Variables
        public string ID { get => id; set => id = value; }
        public string Label { get => label; set => label = value; }
        public string Type { get => type; set => type = value; }
        #endregion

        // Quick access to properties
        // May need to be changed in the future when concidering multiple storage accounts.
        public FileProps properties { get; set; }

        #region "Constructor"
        // Maybe.. maybe....
        //public static FossaFile CreateFile(string id)
        //{
        //    string sFile = GremlinQuery(string.Format("g.addV('file').property('name','{0}').property('createddate','{1}').property('uploadstatus','inprogress')",
        //            fileName, DateTime.Now));
        //    File = JsonConvert.DeserializeObject<FossaFile>(sFile);
        //}
        #endregion

        #region Functions
        public string UploadSAS(Root root)
        {
            string sastoken = StorageHelper.GenerateSASToken(
                                // Storage Account name
                                root.properties.accountname[0]["value"],
                                // Storage Key
                                root.properties.key[0]["value"]
                                );
            try
            {
                return string.Format("{0}/{1}{2}",
                            // Storage Account URL
                            //root.Url
                            root.properties.url[0]["value"],
                            // Name of the file in blob
                            // Later we will need this to be an ID
                            this.ID,
                            // SAS TOKEN
                            sastoken
                            );
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string DownloadSAS(Root root)
        {
            string sastoken = StorageHelper.GenerateSASToken(
                                // Storage Account name
                                root.properties.accountname[0]["value"],
                                // Storage Key
                                root.properties.key[0]["value"]
                                );
            try
            {
                return string.Format("{0}/{1}{2}",
                            // Storage Account URL
                            //root.Url
                            root.properties.url[0]["value"],
                            // Name of the file in blob
                            // Later we will need this to be an ID
                            this.ID,
                            // SAS TOKEN
                            sastoken
                            );
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Internal Classes
        public class FileProps : Model
        {
            public List<Dictionary<string, string>> name { get; set; }
            public List<Dictionary<string, string>> createddate { get; set; }
            public List<Dictionary<string, string>> uploadstatus { get; set; }
        }
        public class FileIds
        {
            public List<Dictionary<string, string>> id { get; set; }
        }
        #endregion
    }

}
