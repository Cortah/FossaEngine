﻿using Newtonsoft.Json;

namespace FossaPoc.Model
{   
    /// <summary>
    /// Literally just made this for a shared to string method...
    /// May be useful for other shared methods later.
    /// </summary>
    public class Model
    {
        #region ToString
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
        #endregion
    }
}
