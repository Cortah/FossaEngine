using System.IO;
using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using FossaPoc.Model;
using FossaPoc.Gremlin;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace FossaPoc
{
    public static class FolderAPI
    {
        private static GremlinUtil gremlinConnection = new GremlinUtil();
        private static string ID;
        private static string Name;
        private static string ParentId;
        private static dynamic Data;

        private static Root root;

        private static FossaFolder Folder = new FossaFolder();
        /// <summary>
        /// Put: Upload file    : C <para/>
        /// Get: Download file  : R <para/>
        /// Post: Update file   : U <para/>
        /// Delete: Delete File : D
        /// </summary>
        /// <param name="req"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        [FunctionName("CreateFolder")]
        public static IActionResult UploadFolder([HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "Folder/Upload" )]HttpRequest req, ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            // initialise data sent
            DataInit(req.Body);

            #region Authentication Handling
            // TODO:
            // Auth handling
            // Generate resource token
            #endregion

            Folder = gremlinConnection.UploadFolder(Name, ParentId);
            log.LogInformation("Uploaded: " + Folder.properties.name[0]["value"]);
            return new OkObjectResult(Folder.ToString());
        }

        [FunctionName("DeleteFolder")]
        public static IActionResult DeleteFolder([HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "Folder/Delete")]HttpRequest req, ILogger log)
        {
            // initialise data sent
            DataInit(req.Body);

            List<FossaFile.FileIds> blobIds = gremlinConnection.DeleteAllBelow(ID);

            bool Deleted = gremlinConnection.DeleteFolder(ID);
            log.LogInformation("Deleted: " + Deleted.ToString());

            string deletedBlobs = "";

            foreach (FossaFile.FileIds item in blobIds)
            {
                if(StorageWorker.Deleteblob(root,item.id[0]["value"]).Result)
                    deletedBlobs += item.id[0]["value"] + ",";
            }

            if (Deleted) return new OkObjectResult("Success following blobs deleted " + deletedBlobs);
            else return new BadRequestObjectResult("Failed to delete");
        }

        [FunctionName("UpdateFolderName")]
        public static IActionResult UpdateFolderName([HttpTrigger(AuthorizationLevel.Anonymous, "put", Route = "Folder/Rename")]HttpRequest req, ILogger log)
        {
            DataInit(req.Body);
            if (gremlinConnection.RenameFolder(ID, Name))
                return new OkObjectResult("Rename successful");
            return new BadRequestObjectResult("Rename unsuccessful");
        }

        [FunctionName("MoveFolder")]
        public static IActionResult MoveFolder([HttpTrigger(AuthorizationLevel.Anonymous, "put", Route = "Folder/Move")]HttpRequest req, ILogger log)
        {
            DataInit(req.Body);
            if (gremlinConnection.MoveFolder(ID, ParentId))
                return new OkObjectResult("Move File Successful");
            return new BadRequestObjectResult("Move File Unsuccessful");
        }
        [FunctionName("ViewFolder")]
        public static IActionResult ViewFolder([HttpTrigger(AuthorizationLevel.Anonymous, "view", Route = "Folder/View")]HttpRequest req, ILogger log)
        {
            // initialise data sent
            DataInit(req.Body);

            log.LogInformation("Method View: " + req.Method);
            Folder.contents = gremlinConnection.ViewFiles(ID);
            return new OkObjectResult(Folder.contents.ToString());
        }
        
        [FunctionName("RetrieveFolders")]
        public static IActionResult RetrieveFolders([HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "Folder")]HttpRequest req, ILogger log)
        {
            Root root = gremlinConnection.GetRoot();
            return new OkObjectResult(root.FolderStructure.ToString());
        }

        [FunctionName("FileChildCount")]
        public static IActionResult FileChildCount([HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "Folder/ChildCount")]HttpRequest req, ILogger log)
        {
            // initialise data sent
            DataInit(req.Body);
            int x = gremlinConnection.filesInChildren(ID);
            return new OkObjectResult(x);
        }

        private static void DataInit(Stream req)
        {
            string body = new StreamReader(req).ReadToEnd();
            // RequestBody = body;
            Data = JsonConvert.DeserializeObject(body);
            // Assign to variables
            ID = Data?.id;
            Name = Data?.name;
            ParentId = Data?.parentId;

            // To be replaced by customer connections
            root = gremlinConnection.GetRoot();
        }
    }
}


