using System.IO;
using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Core;
using FossaPoc.Model;
using FossaPoc.Gremlin;
using System.Collections.Generic;

namespace FossaPoc
{
    public static class FileAPI
    {
        #region Variables
        private static GremlinUtil gremlinConnection = new GremlinUtil();

        private static string ID;
        private static List<string> IDS;
        private static string Name;
        private static string ParentId;
        private static string Tier;

        private static dynamic Data;
        private static Root root;

        private static FossaFile file = new FossaFile();
        #endregion

        /// <summary>
        /// Put: Upload file    : C
        /// Get: Download file  : R
        /// Post: Update file   : U
        /// Delete: Delete File : D
        /// </summary>
        /// <param name="req"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        [FunctionName("CreateFile")]
        public static IActionResult Upload([HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "File/Upload")]HttpRequest req, ILogger log)
        {
            try
            {
                // Get Data from JSON body
                DataInit(req.Body);          
            
                log.LogInformation("Uploading file: " + Name);
                file = UploadFile(Name, ParentId);
                // Later send file.ToString()
                return new OkObjectResult(file.UploadSAS(root));
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(ex.Message);
            }
        }

        [FunctionName("DeleteFile")]
        public static IActionResult Delete([HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "File/Delete")]HttpRequest req, ILogger log)
        {
            try
            {
                // Get Data from JSON body
                DataInit(req.Body);
                file = gremlinConnection.GetFile(ID);
            
                if (StorageWorker.Deleteblob(root, ID).Result)
                    if (gremlinConnection.DeleteFile(ID))
                        return new OkObjectResult("Success");
                    else
                        return new OkObjectResult("Deleted in storage but not on the graph");
                return new BadRequestObjectResult("Failed to delete");
            }
            catch (Exception)
            {
                // maybe
                // log file.ToString()
                return new BadRequestObjectResult("Failed to delete");
            }            
        }
        /// <summary>
        /// Delete Multiple Files
        /// </summary>
        /// <param name="req"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        [FunctionName("DeleteFiles")]
        public static IActionResult BatchDelete([HttpTrigger(AuthorizationLevel.Anonymous,"delete",Route = "File/BatchDelete")]HttpRequest req, ILogger log)
        {
            string DeletedBlobs = "";
            try
            {
                foreach (string id in IDS)
                {
                    DataInit(req.Body);
                    log.LogInformation("Deleting blob:{0}", id);
                    if (StorageWorker.Deleteblob(root, id).Result)
                        if (gremlinConnection.DeleteFile(id))
                        {
                            log.LogInformation("Deleting blob:{0} -- Success", id);
                            DeletedBlobs += id + ",";
                        }
                        else
                            log.LogInformation("Deleting blob:{0} -- Fail", id);
                }
                return new OkObjectResult("Deleted Blobs: " + DeletedBlobs.Remove(DeletedBlobs.Length - 1));
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult("Batch Delete unsuccessful" + ex.Message);
            }
        }

        [FunctionName("RenameFile")]
        public static IActionResult RenameFile([HttpTrigger(AuthorizationLevel.Anonymous, "put", Route = "File/Rename")]HttpRequest req, ILogger log)
        {
            try
            {
                DataInit(req.Body);
                gremlinConnection.RenameFile(ID, Name);
                return new OkObjectResult("Rename successful");
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult("Rename unsuccessful" + ex.Message);
            }
            
     
        }

        [FunctionName("DownloadFile")]
        public static IActionResult Download([HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "File")]HttpRequest req, ILogger log)
        {
            try
            {
                DataInit(req.Body);
                file = gremlinConnection.GetFile(ID);
                return new OkObjectResult(file.DownloadSAS(root));
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult("Download Failed " + ex.Message);
            }
            
        }

        [FunctionName("MoveFile")]
        public static IActionResult MoveFile([HttpTrigger(AuthorizationLevel.Anonymous,"put",Route = "File/Move")]HttpRequest req,ILogger log)
        {
            try
            {
                DataInit(req.Body);
                gremlinConnection.MoveFile(ID, ParentId);
                return new OkObjectResult("Move File Successful");
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult("Move File Unsuccessful" + ex.Message);
            }                       
        }

        [FunctionName("ChangeStorageType")]
        public static IActionResult ChangeStorageType([HttpTrigger(AuthorizationLevel.Anonymous,"put",Route = "File/StorageType")]HttpRequest req, ILogger log)
        {
            DataInit(req.Body);
            Microsoft.WindowsAzure.Storage.Blob.StandardBlobTier tierEnum = Microsoft.WindowsAzure.Storage.Blob.StandardBlobTier.Unknown;
            switch (Tier)
            {
                case "hot":
                    tierEnum = Microsoft.WindowsAzure.Storage.Blob.StandardBlobTier.Hot;
                    break;
                case "cool":
                    tierEnum = Microsoft.WindowsAzure.Storage.Blob.StandardBlobTier.Cool;
                    break;
                case "archive":
                    tierEnum = Microsoft.WindowsAzure.Storage.Blob.StandardBlobTier.Archive;
                    break;
            }
            if (tierEnum != Microsoft.WindowsAzure.Storage.Blob.StandardBlobTier.Unknown)
            {
                StorageWorker.ChangeStorageType(root, ID, tierEnum);
                return new OkObjectResult("Teir Change Successful.");
            }
            return new BadRequestObjectResult("Wrong tier type.");
        }

        #region Action Functions
        /* LogObject
         * later we will pass a log object for any logs we need to write.
         */
        private static FossaFile UploadFile(string fileName, string parentId)
        {
            if (fileName != null && parentId != null)
            {
                FossaFile file = null;
                try
                {
                    // Create file and attach it to parent folder
                    file = gremlinConnection.UploadFile(fileName, parentId);
                    Root root = gremlinConnection.GetRoot();
                    file.UploadSAS(root);

                    // Change to json
                    return file;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
                throw new Exception("fileName or parentId cannot be null");
        }
        #endregion

        #region Internal Functions
        private static void DataInit(Stream req)
        {            
            // Get data
            string body = new StreamReader(req).ReadToEnd();
                        
            Data = JsonConvert.DeserializeObject(body);
            
            // Assign to variables
            ID = Data?.id;
            if(Data?.ids != null)
                IDS = Data?.ids.ToObject<List<string>>();
            Name = Data?.name;
            ParentId = Data?.parentId;
            Tier = Data?.tier;

            // To be replaced by customer connections
            root = gremlinConnection.GetRoot();
        }
        #endregion

    }

}
