﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Diagnostics;

namespace FossaPoc
{
    class StorageHelper
    {

        public CloudStorageAccount ConnectToStorageAccount(string AccountName, string AccountKey)
        {
            CloudStorageAccount stoAccount;

            string ConnectionString = string.Format("DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1};EndpointSuffix=core.windows.net", AccountName, AccountKey);
            stoAccount = CloudStorageAccount.Parse(ConnectionString);
            return stoAccount;

        }

        public List<CloudBlobContainer> ListContainers(CloudStorageAccount StoAccount)
        {
            List<CloudBlobContainer> containerList = new List<CloudBlobContainer>();
            CloudBlobClient client = StoAccount.CreateCloudBlobClient();
            BlobContinuationToken continuationToken = null;

            var resultSegment = client.ListContainersSegmentedAsync(continuationToken);

            foreach (CloudBlobContainer container in resultSegment.Result.Results)
            {
                containerList.Add(container);
                Debug.Write(container.Name);
            }
            
            
            return containerList;
        }

        public List<CloudBlockBlob> ListBlobs(CloudStorageAccount StoAccount)
        {
            BlobContinuationToken continuationToken = null;
            CloudBlobClient client = StoAccount.CreateCloudBlobClient();
            List<CloudBlockBlob> blobList = new List<CloudBlockBlob>();
            List<CloudBlobContainer> ContainerList = ListContainers(StoAccount);

            foreach (CloudBlobContainer container in ContainerList)
            {

                var blobresultsegment =  container.ListBlobsSegmentedAsync(continuationToken);

                foreach(CloudBlockBlob blob in blobresultsegment.Result.Results)
                {

                    blobList.Add(blob);

                }

            }

            return blobList;
        }



    }

}
