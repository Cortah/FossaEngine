using System.IO;
using FossaPoc.Gremlin;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;

namespace FossaPoc
{
    public static class BlobTrigger
    {
        private static GremlinUtil gremlinConnection = new GremlinUtil();
        [FunctionName("UploadTrigger")]
        public static void Run([BlobTrigger("root/{name}", Connection = "StorageConnection")]Stream myBlob, string name, ILogger log)
        {
            log.LogInformation($"C# Blob trigger function Processed blob\n Name:{name} \n Size: {myBlob.Length} Bytes");
            gremlinConnection.ConfirmUpload(name);
        }
    }
}
