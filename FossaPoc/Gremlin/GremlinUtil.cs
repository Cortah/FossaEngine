﻿using Gremlin.Net.Structure;
using Gremlin.Net;
using Gremlin.Net.Driver;
using Gremlin.Net.Structure.IO.GraphSON;
using Newtonsoft.Json;
using System;
using Gremlin.Net.Process.Traversal;
using Gremlin.Net.Driver.Remote;
using FossaPoc.Model;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using static Gremlin.Net.Process.Traversal.P;
using static Gremlin.Net.Process.Traversal.Order;

namespace FossaPoc.Gremlin
{
    class GremlinUtil
    {
        private static string HostName = "fossatestdb.gremlin.cosmosdb.azure.com";
        private static int Port = 443;
        private static string AuthKey = "OmuJPjTJUQX2fpRHJzlTdTKdPWyVwPdAt1rw5Cl3eP4vwFNh4WkksbmPKDupSfBhhw0k7dgHSbzEWTQufxh4Ag==";
        private static string Database = "Fossa";
        private static string Collection = "fsdb01";
        // For debug perpouses properties
        private string UserName { get; set; }
        private bool EnableSsl { get; set; }
        ///// <summary>
        ///// Idk lol maybe  we will find out later
        ///// </summary>
        //private Graph Cosmos { get; set; }
        //private DriverRemoteConnection remoteConnection { get; set; }
        /// <summary>
        /// Connection object for the server that the gremlin client is sitting on
        /// </summary>
        private GremlinServer gremlinServer { get; set; }
        /// <summary>
        /// This is the Gremlin client that allows us to connect to our graph
        /// </summary>        
        private GremlinClient gremlinClient { get; set; }
        ///// <summary>
        ///// For reading graphs created by gremlin.
        ///// </summary>
        //private GraphSONReader GraphReader { get; set; }
        ///// <summary>
        ///// Pls MichaelSoft pls fix pls
        ///// </summary>
        //private GraphTraversalSource g { get; set; }
        #region Graph stuff


        /// <summary>
        /// This needs to be updated later when we do auth
        /// It will take the auth key and connect to the correct database they want to use
        /// </summary>
        /// <param name="hostName"></param>
        /// <param name="userName"></param>
        /// <param name="authKey"></param>
        /// <param name="database"></param>
        /// <param name="collection"></param>
        /// <param name="enableSsl"></param>
        /// <param name="port"></param>
        public GremlinUtil()
        {
            UserName = "/dbs/" + Database + "/colls/" + Collection;

            gremlinServer = new GremlinServer(
                HostName,
                Port,
                true,
                username: UserName,
                password: AuthKey
                );

            // Gremlin init
            gremlinClient = new GremlinClient(gremlinServer, new GraphSON2Reader(), new GraphSON2Writer(), GremlinClient.GraphSON2MimeType);
            //Cosmos Graph
            //Cosmos = new Graph();
            //remoteConnection = new DriverRemoteConnection(gremlinClient);
            //g = Cosmos.Traversal().WithRemote(remoteConnection);
        }

        // To be replaced when we get better a gremlin.net
        private List<string> GremlinQuery(string query)
        {
            try
            {
                List<string> results = new List<string>();
                var task = gremlinClient.SubmitAsync<dynamic>(query);
                task.Wait();
                foreach (var item in task.Result)
                {
                    results.Add(JsonConvert.SerializeObject(item));
                }
                return results;
            }
            catch(Exception ex) {
                throw new Exception("No results");
            }
        }
        #endregion

        #region File Queries
        /// <summary>
        /// Adds a file vertex to the database with upload status pending...
        /// </summary>
        /// <param name="Id">Name of the file in the db.</param>
        /// <param name="parentFolderId">Parent Folder ID.</param>
        public FossaFile UploadFile(string Id, string parentFolderId)
        {
            FossaFile File = new FossaFile();
            try
            {
                // Add the file to the DB
                List<string> sFile = GremlinQuery(string.Format("g.addV('file').property('name','{0}').property('createddate','{1}').property('uploadstatus','inprogress')",
                    Id, DateTime.Now));
                File = JsonConvert.DeserializeObject<FossaFile>(sFile[0]);
                // Attach the file above to parent folder
                GremlinQuery(string.Format("g.v('{0}').addE('filechild').to(g.v('{1}'))", parentFolderId, File.ID));
            }
            catch (Exception ex)
            {
                // If the file was added but not attached to a parent folder delete the file.
                if (File != null) DeleteFile(File.ID);
                throw new Exception(string.Format("Failed to link to parent folder."));
                throw ex;
            }            
            return File;
        }
        public FossaFile GetFile(string id)
        {
            return JsonConvert.DeserializeObject<FossaFile>(
                GremlinQuery(string.Format("g.V('{0}')", id))
                // this 0 below is the indexer for the list the query is returning
                [0]);
        }
        /// <summary>
        /// Deletes a file vertex from database.
        /// </summary>
        /// <param name="folderId">Id of the folder to be deleted.</param>
        public bool DeleteFile(string fileId)
        {
            try
            {
                GremlinQuery(string.Format("g.v('{0}').haslabel('file').Drop()", fileId));
                return true;
            }
            catch (Exception)
            {
                return false;
            }            
        }
        /// <summary>
        /// Changes the name of a File in the graph
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        public bool RenameFile(string id,string name)
        {
            FossaFile File = JsonConvert.DeserializeObject<FossaFile>(
                GremlinQuery(string.Format("g.V('{0}').property('name','{1}')", id,name))
                // this 0 below is the indexer for the list the query is returning
                [0]);
            if (File.properties.name[0]["value"] == name)
                return true;
            else return false;
        }
        /// <summary>
        /// Move file from one folder to another
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public bool MoveFile(string fileId,string parentId)
        {
            Model.EdgeModel e = JsonConvert.DeserializeObject<Model.EdgeModel>(
                GremlinQuery(string.Format("g.V('{0}').inE('filechild')",fileId))
                [0]);
            try
            {
                GremlinQuery(string.Format("g.V('{0}').inE('filechild').drop()", fileId));
            }
            catch (Exception)
            {
                return false;
            }
            try
            {
                GremlinQuery(string.Format("g.v('{0}').addE('filechild').to(g.v('{1}'))", parentId, fileId));
            }
            catch (Exception)
            {
                GremlinQuery(string.Format("g.v('{0}').addE('filechild').to(g.v('{1}'))", e.outV, fileId));
                return false;
            }
            return true;
        }
        /// <summary>
        /// Deletes Uploadstatus pending from the vertex
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool ConfirmUpload(string id)
        {
            GremlinQuery(string.Format("g.V('{0}').properties('uploadstatus').drop()",id));
            return true;
        }
        #endregion

        #region Folder Queries
        /* LogObject
         * later we will pass a log object for any logs we need to write.
         */
        /// <summary>
        /// Adds a folder vertex to the database.
        /// </summary>
        /// <param name="folderName">Name of the folder.</param>
        /// <param name="parentFolderId"></param>
        public FossaFolder UploadFolder(string folderName, string parentFolderId)
        {
            FossaFolder Folder = null;
            try
            {
                // Create folder in DB
                List<string> folderAdded = GremlinQuery(string.Format("g.addV('folder').property('name','{0}').property('createddate','{1}')",
                    folderName, DateTime.Now));
                Folder = JsonConvert.DeserializeObject<FossaFolder>(folderAdded[0]);
                // Add folder to parent folder
                GremlinQuery(string.Format("g.v('{0}').addE('folderchild').to(g.v('{1}'))", parentFolderId, Folder.id));                
            }
            catch (Exception)
            {
                if (Folder != null) DeleteFolder(Folder.id);
                return null;
            }
            return Folder;
        }
        /// <summary>
        /// Gets the files in a folder
        /// </summary>
        /// <returns></returns>
        public FossaFolder.FileContents ViewFiles(string FolderID)
        {
            FossaFolder.FileContents contents = new FossaFolder.FileContents();
            List<string> files = GremlinQuery(string.Format("g.v('{0}').out('filechild')",FolderID));
            foreach (string file in files)
            {
                contents.files.Add(JsonConvert.DeserializeObject<FossaFile>(file));
            }
            return contents;
        }

        public List<string> GetChildIds()
        {
            return new List<string>();
        }

        /// <summary>
        /// Deletes a folder vertex from database.
        /// </summary>
        /// <param name="folderId">Id of the folder to be deleted.</param>
        public bool DeleteFolder(string folderId)
        {
            try
            {
                GremlinQuery(string.Format("g.v('{0}').haslabel('folder').Drop()", folderId));
                return true;
            }
            catch (Exception)
            {
                return false;                
            }
        }
        /// <summary>
        /// Moves the folder to another parent Folder
        /// </summary>
        /// <param name="FolderId"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public bool MoveFolder(string FolderId, string parentId)
        {
            Model.EdgeModel e = JsonConvert.DeserializeObject<Model.EdgeModel>(
                GremlinQuery(string.Format("g.V('{0}').inE('folderchild')", FolderId))
                [0]);
            try
            {
                GremlinQuery(string.Format("g.V('{0}').inE('folderchild').drop()", FolderId));
            }
            catch (Exception)
            {
                return false;
            }
            try
            {
                GremlinQuery(string.Format("g.v('{0}').addE('folderchild').to(g.v('{1}'))", parentId, FolderId));
            }
            catch (Exception)
            {
                GremlinQuery(string.Format("g.v('{0}').addE('folderchild').to(g.v('{1}'))", e.outV, FolderId));
                return false;
            }
            return true;
        }
        /// <summary>
        /// Renames the folder.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool RenameFolder(string id, string name)
        {
            FossaFolder Folder = JsonConvert.DeserializeObject<FossaFolder>(
                GremlinQuery(string.Format("g.V('{0}').property('name','{1}')", id, name))
                // this 0 below is the indexer for the list the query is returning
                [0]);
            if (Folder.properties.name[0]["value"] == name)
                return true;
            else return false;
        }
        /// <summary>
        /// Counts the number of files in the folder and all it's child folders and their child folders.
        /// </summary>
        /// <returns></returns>
        public int filesInChildren(string id)
        {
            List<string> count = GremlinQuery(string.Format("g.V('{0}').emit().repeat(out()).repeat(out('filechild')).emit().count()", id));
            return int.Parse(count[0]);
        }
        // Delete all child files/folders of a folder recursively
        public List<FossaFile.FileIds> DeleteAllBelow(string id)
        {
            List<FossaFile.FileIds> fileIds = new List<FossaFile.FileIds>();
            List<string> gremlinIds = GremlinQuery(string.Format("g.V({0}).emit().repeat(out()).haslabel('file').properties('id')", id));

            foreach (var item in gremlinIds)
            {
                fileIds.Add(JsonConvert.DeserializeObject<FossaFile.FileIds>(item));
            }
            GremlinQuery(string.Format("g.V('{0}').emit().repeat(out()).fold().unfold().drop()", id));

            return fileIds;
        }
        #endregion

        #region Root Queries
        /// <summary>
        /// Gets the root folder and all of it's properties.
        /// </summary>
        /// <returns></returns>
        public Root GetRoot()
        {
            List<string> rootData = GremlinQuery("g.v('root')");
            Root root = JsonConvert.DeserializeObject<Root>(rootData[0]);
            root.FolderStructure = GetFolderStruct();
            return root;
        }
        /// <summary>
        /// This can be used to retrieve the folder structure of the root.
        /// </summary>
        /// <returns></returns>
        public Root.FolderPath GetFolderStruct()
        {
            Root.FolderPath folderstruct = new Root.FolderPath();
            try
            {                
                List<string> Folders = GremlinQuery("g.v().haslabel('folder').has('id',neq('root'))");
                List<string> ParentEdges = GremlinQuery("g.v().haslabel('folder').inE()");
                for (int i = 0; i < Folders.Count; i++)
                {
                    folderstruct.path.Add(JsonConvert.DeserializeObject<FossaFolder>(Folders[i]));
                    folderstruct.path[i].ParentEdge = JsonConvert.DeserializeObject<EdgeModel>(ParentEdges[i]);
                }                
            }
            catch (Exception)
            {
                new Exception("Possible folder missing parentEdge");
            }
            return folderstruct;
        }
        #endregion
    }
}
