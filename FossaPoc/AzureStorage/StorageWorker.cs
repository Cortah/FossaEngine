﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Auth;
using System.Threading.Tasks;
using FossaPoc.Model;

namespace FossaPoc
{
    public static class StorageWorker
    {
        /// <summary>
        /// Delete Blob
        /// </summary>
        /// <param name="root"></param>
        /// <param name="BlobName"></param>
        /// <returns></returns>
        public static async Task<bool> Deleteblob(Root root, string BlobName)
        {
            CloudStorageAccount storageAccount = StorageHelper.ConnectToStorageAccount(root.properties.AccountName(), root.properties.Key());
            CloudBlockBlob Blob = storageAccount.CreateCloudBlobClient().GetContainerReference(root.properties.Name()).GetBlockBlobReference(BlobName);
            return await Blob.DeleteIfExistsAsync();
        }
        /// <summary>
        /// Changes Storage type hot/cold/archive
        /// </summary>
        /// <param name="root"></param>
        /// <param name="blobName"></param>
        /// <param name="tier"></param>
        public static void ChangeStorageType(Root root, string blobName, StandardBlobTier tier)
        {
            CloudStorageAccount storageAccount = StorageHelper.ConnectToStorageAccount(root.properties.AccountName(), root.properties.Key());
            CloudBlockBlob Blob = storageAccount.CreateCloudBlobClient().GetContainerReference(root.properties.Name()).GetBlockBlobReference(blobName);
            Blob.SetStandardBlobTierAsync(tier);            
        }

        public static bool BlobExists(Root root, string blobName)
        {
            CloudStorageAccount storageAccount = StorageHelper.ConnectToStorageAccount(root.properties.AccountName(), root.properties.Key());
            CloudBlockBlob Blob = storageAccount.CreateCloudBlobClient().GetContainerReference(root.properties.Name()).GetBlockBlobReference(blobName);
            Blob.ExistsAsync();
            return true;
        }
    }


}

