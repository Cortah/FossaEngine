﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Diagnostics;

namespace FossaPoc
{
    public static class StorageHelper
    {
        public static CloudStorageAccount ConnectToStorageAccount(string AccountName, string AccountKey)
        {            
            return CloudStorageAccount.Parse(
                string.Format("DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1};EndpointSuffix=core.windows.net",
                AccountName, AccountKey
                )
            );
        }

        public static string GenerateSASToken (string AccountName, string AccountKey)
        {
            string ConnectionString = string.Format("DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1};EndpointSuffix=core.windows.net", AccountName, AccountKey);
            CloudStorageAccount DestAccount = CloudStorageAccount.Parse(ConnectionString);

            SharedAccessAccountPolicy policy = new SharedAccessAccountPolicy()
            {
                Permissions = SharedAccessAccountPermissions.Read | SharedAccessAccountPermissions.Write | SharedAccessAccountPermissions.List,
                Services = SharedAccessAccountServices.Blob | SharedAccessAccountServices.File,
                ResourceTypes = SharedAccessAccountResourceTypes.Container | SharedAccessAccountResourceTypes.Object | SharedAccessAccountResourceTypes.Service,
                SharedAccessExpiryTime = DateTime.UtcNow.AddHours(1),
                Protocols = SharedAccessProtocol.HttpsOnly
            };
            return DestAccount.GetSharedAccessSignature(policy);
        }
    }

}
